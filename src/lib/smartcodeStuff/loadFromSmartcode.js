import axios from "axios"
import {GET_USER_PROJECTS, SMARTCODE_URL} from "../../graphql/gql_strings"

export default (menuOpen) => {
    const loadProject = (input) => axios({
        url: SMARTCODE_URL,
        method: 'POST',
        data: {
            query: GET_USER_PROJECTS,
            variables: {input}
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const {data: {data: {getProjectsByUser: {_status, _data}}}} = result
        return {_status, _data}
    })

    const input = {
        projType: "scratch"
    }

    if (menuOpen) {
        return loadProject(input)
    }
};
