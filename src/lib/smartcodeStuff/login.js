import axios from "axios"
import {LOGIN, SMARTCODE_URL} from "../../graphql/gql_strings"

export default (username, password, alert) => {
    const login = (input) => axios({
        url: SMARTCODE_URL,
        method: 'POST',
        data: {
            query: LOGIN,
            variables: {input}
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const {data: {data: {login: {_status, _data}}}} = result
        return {_status, _data}
    })

    const input = {
        username,
        password
    }

    return login(input)
}