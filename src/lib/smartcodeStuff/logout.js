import axios from "axios"
import {LOGOUT, SMARTCODE_URL} from "../../graphql/gql_strings"

export default (username, password, alert) => {
    const logout = () => axios({
        url: SMARTCODE_URL,
        method: 'POST',
        data: {
            query: LOGOUT
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const {data: {data: {logout: {_status, _data}}}} = result
        return {_status, _data}
    })

    return logout()
}