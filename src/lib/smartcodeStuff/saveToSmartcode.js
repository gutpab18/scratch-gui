import axios from "axios"
import {SAVE_PROJECT_CODE, SMARTCODE_URL} from "../../graphql/gql_strings"


export default (input, alert) => {

    if (input == null) {
        alert("Cannot Save Demo Project", "Please create a new project or load an existing one", "failure")
        return
    }

    axios({
        url: SMARTCODE_URL,
        method: 'post',
        data: {
            query: SAVE_PROJECT_CODE,
            variables: { input }
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const { data: { data: { updateProject: { _status, _data } } } } = result
        
        if (_status.code == 200) {
            alert("Project Saved", "The project has been saved successfuly", "success")
        } else {
            alert("Error", "Error saving project. Please try again later", "failure")
        }
    })
};