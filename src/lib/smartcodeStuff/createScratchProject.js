import axios from "axios"
import {CREATE_PROJECT, SMARTCODE_URL} from "../../graphql/gql_strings"

export default (projName, projDescription) => {

    const createProject = (input) => axios({
        url: SMARTCODE_URL,
        method: 'POST',
        data: {
            query: CREATE_PROJECT,
            variables: {input}
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const {data: {data: {createProject: {_status, _data}}}} = result
        
        return {_status, _data}
    })

    const input = {
        projName,
        projDescription,
        projType: "scratch"
    }

    return createProject(input)
};
