import axios from "axios"
import {CHECK_SESSION, SMARTCODE_URL} from "../../graphql/gql_strings"

export default () => {
    const checkSession = () => axios({
        url: SMARTCODE_URL,
        method: 'POST',
        data: {
            query: CHECK_SESSION
        },
        crossDomain: true,
        withCredentials: true
    }).then((result) => {
        const {data: {data: {checkSession: {_status, _data}}}} = result
        return {_status, _data}
    })

    return checkSession()
}