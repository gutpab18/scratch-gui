const OPEN_MENU = 'scratch-gui/menus/OPEN_MENU';
const CLOSE_MENU = 'scratch-gui/menus/CLOSE_MENU';

const MENU_ACCOUNT = 'accountMenu';
const MENU_FILE = 'fileMenu';
const MENU_NEW_PROJECT = 'newProjectMenu';
const MENU_OPEN_PROJECT = 'openProjectMenu';
const MENU_EDIT = 'editMenu';
const MENU_LANGUAGE = 'languageMenu';
const MENU_LOGIN = 'loginMenu';
const MENU_LOGOUT = 'logoutMenu';
const MENU_LOGIN_INPUT = 'inputLoginMenu';


const initialState = {
    [MENU_ACCOUNT]: false,
    [MENU_FILE]: false,
    [MENU_NEW_PROJECT]: false,
    [MENU_OPEN_PROJECT]: false,
    [MENU_EDIT]: false,
    [MENU_LANGUAGE]: false,
    [MENU_LOGIN]: false,
    [MENU_LOGOUT]: false,
    [MENU_LOGIN_INPUT]: false
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
    case OPEN_MENU:
        return Object.assign({}, state, {
            [action.menu]: true
        });
    case CLOSE_MENU:
        
        if ((action.menu === "fileMenu" && state.newProjectMenu) || 
            (action.menu === "fileMenu" && state.openProjectMenu)) {
            return state
        }
        return Object.assign({}, state, {
            [action.menu]: false
        });
    default:
        return state;
    }
};
const openMenu = menu => ({
    type: OPEN_MENU,
    menu: menu
});
const closeMenu = menu => ({
    type: CLOSE_MENU,
    menu: menu
});
const openAccountMenu = () => openMenu(MENU_ACCOUNT);
const closeAccountMenu = () => closeMenu(MENU_ACCOUNT);
const accountMenuOpen = state => state.scratchGui.menus[MENU_ACCOUNT];
const openFileMenu = () => openMenu(MENU_FILE);
const closeFileMenu = () => closeMenu(MENU_FILE);
const fileMenuOpen = state => state.scratchGui.menus[MENU_FILE];
const openEditMenu = () => openMenu(MENU_EDIT);
const closeEditMenu = () => closeMenu(MENU_EDIT);
const editMenuOpen = state => state.scratchGui.menus[MENU_EDIT];
const openLanguageMenu = () => openMenu(MENU_LANGUAGE);
const closeLanguageMenu = () => closeMenu(MENU_LANGUAGE);
const languageMenuOpen = state => state.scratchGui.menus[MENU_LANGUAGE];
const openNewProjectMenu = () => openMenu(MENU_NEW_PROJECT);
const closeNewProjectMenu = () => closeMenu(MENU_NEW_PROJECT);
const newProjectMenuOpen = state => state.scratchGui.menus[MENU_NEW_PROJECT];
const openLoadProjectMenu = () => openMenu(MENU_OPEN_PROJECT);
const closeLoadProjectMenu = () => closeMenu(MENU_OPEN_PROJECT);
const loadProjectMenuOpen = state => state.scratchGui.menus[MENU_OPEN_PROJECT];
const openInputLoginMenu = () => openMenu(MENU_LOGIN_INPUT);
const closeInputLoginMenu = () => closeMenu(MENU_LOGIN_INPUT);
const inputLoginMenuOpen = state => state.scratchGui.menus[MENU_LOGIN_INPUT];
const openLoginMenu = () => openMenu(MENU_LOGIN);
const closeLoginMenu = () => closeMenu(MENU_LOGIN);
const loginMenuOpen = state => state.scratchGui.menus[MENU_LOGIN];
const openLogoutMenu = () => openMenu(MENU_LOGOUT);
const closeLogoutMenu = () => closeMenu(MENU_LOGOUT);
const logoutMenuOpen = state => state.scratchGui.menus[MENU_LOGOUT];

export {
    reducer as default,
    initialState as menuInitialState,
    
    openAccountMenu,
    closeAccountMenu,
    accountMenuOpen,
    
    openFileMenu,
    closeFileMenu,
    fileMenuOpen,
    
    openEditMenu,
    closeEditMenu,
    editMenuOpen,
    
    openLanguageMenu,
    closeLanguageMenu,
    languageMenuOpen,
    
    openLoginMenu,
    closeLoginMenu,
    loginMenuOpen,
    
    openNewProjectMenu,
    closeNewProjectMenu,
    newProjectMenuOpen,
    
    openLoadProjectMenu,
    closeLoadProjectMenu,
    loadProjectMenuOpen,

    openInputLoginMenu,
    closeInputLoginMenu,
    inputLoginMenuOpen,

    openLogoutMenu,
    closeLogoutMenu,
    logoutMenuOpen
};
