import bindAll from 'lodash.bindall';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { projectTitleInitialState } from '../reducers/project-title';
import axios from "axios"
import downloadBlob from '../lib/download-blob';
import saveToSmartcode from '../lib/smartcodeStuff/saveToSmartcode'
import {showAlert} from "../reducers/alert-ui"
import { SAVE_PROJECT_CODE } from "../graphql/gql_strings"
/**
 * Project saver component passes a downloadProject function to its child.
 * It expects this child to be a function with the signature
 *     function (downloadProject, props) {}
 * The component can then be used to attach project saving functionality
 * to any other component:
 *
 * <SB3Downloader>{(downloadProject, props) => (
 *     <MyCoolComponent
 *         onClick={downloadProject}
 *         {...props}
 *     />
 * )}</SB3Downloader>
 */


class SB3Downloader extends React.Component {
    constructor(props) {
        super(props);
        bindAll(this, [
            'downloadProject',
            'getProjectCode'
        ]);
    }
    downloadProject() {
        this.props.saveProjectSb3().then(content => {
            if (this.props.onSaveFinished) {
                this.props.onSaveFinished();
            }
            downloadBlob(this.props.projectFilename, content);
        });
    }


    getProjectCode() {
        this.props.saveProjectSb3().then(content => {
            // for some reason these wont be inscope of the 
            // onloadend function if we dont assign them to variables
            const id = this.props.projState.projectId
            const alert_func = this.props.alert

            var reader = new FileReader();
            reader.readAsDataURL(content);
            reader.onloadend = function () {
                var b64 = reader.result.replace(/^data:.+;base64,/, '');

                let input = {
                    _projID: id,
                    projCode: b64,
                    projType: "scratch"
                }

                if (id == 0) {
                    input = null
                }

                saveToSmartcode(input, alert_func)
            }
        })
    }

    render() {
        const {
            children
        } = this.props;
        return children(
            this.props.className,
            this.downloadProject,
            this.getProjectCode
        );
    }
}

const getProjectFilename = (curTitle, defaultTitle) => {
    let filenameTitle = curTitle;
    if (!filenameTitle || filenameTitle.length === 0) {
        filenameTitle = defaultTitle;
    }
    return `${filenameTitle.substring(0, 100)}.sb3`;
};

SB3Downloader.propTypes = {
    children: PropTypes.func,
    className: PropTypes.string,
    onSaveFinished: PropTypes.func,
    projectFilename: PropTypes.string,
    saveProjectSb3: PropTypes.func
};
SB3Downloader.defaultProps = {
    className: ''
};

const mapStateToProps = state => ({
    saveProjectSb3: state.scratchGui.vm.saveProjectSb3.bind(state.scratchGui.vm),
    projectFilename: getProjectFilename(state.scratchGui.projectTitle, projectTitleInitialState),
    projState: state.scratchGui.projectState
});

const mapDispatchToProps = dispatch => ({
    alert: (header, description, variant) => dispatch(showAlert(header, description, variant))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SB3Downloader);
