export const SMARTCODE_URL = process.env.SMARTCODE_URL

export const SAVE_PROJECT_CODE = `
mutation SAVE_SCRATCH_PROJECT($input: iUpdateProject){
    updateProject(input: $input) {
        _status {
            code
            msg
        }
        _data {
            _projID
            projName
            projCode
            projDescription
        }
    }
}
`;

export const LOAD_PROJECT_CODE = `
query LOAD_PROJECT_CODE($input: iGetProject){
    getProjectById(input: $input) {
        _status {
            code
            msg
        }
        _data {
            _projID
            projName
            projCode
            projDescription
        }
    }
}
`

export const CREATE_PROJECT = `
mutation CREATE_SCRATCH_PROJECT($input: iCreateProject){
    createProject(input: $input) {
        _status {
            code
            msg
        }
        _data {
            projName
            projCode
            projDescription
            _projID
        }
    }
}
`

export const GET_USER_PROJECTS = `
query GET_ALL_USER_PROJECTS($input:iGetUserProjects){
    getProjectsByUser(input: $input){
        _status {
            code
            msg
        }
        _data {
            _projID
            projName
            projCode
            projDescription
        }
    }
}
`;

export const LOGIN = `
mutation LOGIN($input: iLogin){
    login (input: $input) {
    _status {
        code
        msg
    }
    _data
    }
}
`

export const LOGOUT = `
mutation {
    logout{
        _status {
            code
            msg
        }
    }
}
`

export const CHECK_SESSION = `
query {
    checkSession{
        _status{
            code
            msg
        }
        _data{
            isUser
            user
        }
    }
}
`

export const TEST = `
    query {
        test
    }
`
