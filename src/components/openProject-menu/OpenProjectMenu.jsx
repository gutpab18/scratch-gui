import React from "react"
import Modal from "react-modal"
import log from '../../lib/log';
import { connect } from "react-redux"
import { loadProjectMenuOpen, closeLoadProjectMenu, closeNewProjectMenu } from "../../reducers/menus"
import { setProjectTitle } from "../../reducers/project-title"
import { Form, Button } from "react-bootstrap"
import { closeLoadingProject, openLoadingProject } from '../../reducers/modals';
import { Formik } from "formik";
import { PacmanLoader } from "react-spinners"
import { setSmartcodeProjectId, startLoadingUserProjects, finishLoadingUserProjects } from "../../reducers/project-state"
import loadUserProjects from "../../lib/smartcodeStuff/loadFromSmartcode"
import styles from "./openProjectMenu.css"
import { List, ListItem, ListItemText } from "@material-ui/core"
import { showAlert } from "../../reducers/alert-ui"

Modal.setAppElement('#main')

const ModalStyle = {
    overlay: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 1000
    },
    content: {
        position: "absolute",
        top: '50%',
        left: '50%',
        width: '700px',
        transform: 'translate(-50%, -50%)',
        zIndex: 1000
    }
}

const pacCSS = `
    margin: 120px auto 60px auto;
    transform: translate(-50px, 0px);
`;


let data = []
let status = {}

const OpenProjectMenu = (props) => {

    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const {
        alert,
        LoadProjectMenuOpen,
        closeLoadProjectMenu,
        LoadProjectsState,
        startLoading,
        finishLoading,
        vm,
        onLoadingStarted,
        onLoadingFinished,
        onUpdateProjectTitle,
        loadingState,
        setProjId
    } = props

    const handleClose = () => {
        closeLoadProjectMenu()
    }

    const createBlob = (projCode) => {
        const byteString = atob(projCode)

        // create a blob
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);

        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ab], { type: 'binary/octet-stream' });
    }

    const handleLoadProject = () => {

        const selectedProject = data.filter((proj) => proj._projID === selectedIndex)
        const { _projID, projCode, projName } = selectedProject[0]

        closeLoadProjectMenu()

        // create the blob
        const blob = createBlob(projCode)

        // load the blob
        const reader = new FileReader()
        reader.readAsArrayBuffer(blob);
        reader.onload = ({ target: { result } }) => {
            onLoadingStarted()
            vm.loadProject(result)
                .then(() => {
                    onLoadingFinished(loadingState, true);

                    onUpdateProjectTitle(projName);
                    setProjId(_projID)

                })
                .catch(error => {
                    console.log(error)
                    log.warn(error);
                    // Reset the file input after project is loaded
                    // This is necessary in case the user wants to reload a project
                    onLoadingFinished(loadingState, false);
                });
        }
    }

    const itsClicked = (event, _projID) => {
        setSelectedIndex(_projID);
    }

    const afterOpen = () => {
        startLoading()
        loadUserProjects(LoadProjectMenuOpen).then(({ _status, _data }) => {

            if (_status.code == 200) {
                
                if(_data.length == 0){
                    alert("No Projects", "You have no projects please create one", "failure")
                    closeLoading()
                } else {
                    status = _status
                    data = _data
                    finishLoading()
                }

            } else {
                
                if (_status.code == 401) {
                    alert("Not logged in", "You are not logged in. please login to your account to create a new project", "failure")
                } else {
                    alert("Error!!!", " Cannot create new project!!! Please try again later", "failure")
                } 

                closeLoading()
            }
        })
    }

    const closeLoading = () => {
        closeLoadProjectMenu()
        finishLoading()
    }

    return (
        <div>

            <Modal
                isOpen={LoadProjectMenuOpen}
                onAfterOpen={afterOpen}
                style={ModalStyle}
                onRequestClose={handleClose}
                shouldCloseOnOverlayClick={false}
            >
                {
                    LoadProjectsState ?
                        <div className={styles.pacContainer}>
                            <PacmanLoader
                                size={40}
                                css={pacCSS}
                            />
                            <h1> Loading... </h1>
                            <Button className={styles.pacButton} onClick={closeLoading}> Cancel </Button>
                        </div>
                        :
                        <Formik
                            onSubmit={handleLoadProject}
                            initialValues={data}
                        >
                            {({ errors, status, isValid, touched,
                                handleSubmit, handleChange,
                                values }) => (
                                    <Form onSubmit={handleSubmit}>

                                        <div>
                                            <List className={styles.listContainer}>
                                                {data.map(proj => {
                                                    return (
                                                        <ListItem
                                                            button
                                                            selected={selectedIndex === proj._projID}
                                                            key={proj._projID}
                                                            onClick={(event) => itsClicked(event, proj._projID)}
                                                        >
                                                            <ListItemText
                                                                primary={proj.projName}
                                                                secondary={proj.projDescription}
                                                            />
                                                        </ListItem>
                                                    )
                                                })}
                                            </List>
                                        </div>
                                        <Form.Row className={styles.buttonRow}>
                                            <button onClick={handleClose} className={styles.cancelButton}>
                                                Cancel
                                            </button>
                                            <button type="submit" onClick={handleSubmit} className={styles.submitButton}>
                                                Open
                                            </button>
                                        </Form.Row>
                                    </Form>
                                )}
                        </Formik>
                }
            </Modal>

        </div>
    )
}

const mapStateToProps = state => ({
    LoadProjectMenuOpen: loadProjectMenuOpen(state),
    LoadProjectsState: state.scratchGui.projectState.loadingProjectData,
    loadingState: state.scratchGui.projectState.loadingState,
    vm: state.scratchGui.vm
})

const mapDispatchToProps = dispatch => ({
    alert: (header, description, variant) => dispatch(showAlert(header, description, variant)),
    closeLoadProjectMenu: () => dispatch(closeLoadProjectMenu()),
    onLoadingFinished: () => dispatch(closeLoadingProject()),
    onLoadingStarted: () => dispatch(openLoadingProject()),
    onUpdateProjectTitle: title => dispatch(setProjectTitle(title)),
    setProjId: projectId => dispatch(setSmartcodeProjectId(projectId)),
    startLoading: () => dispatch(startLoadingUserProjects()),
    finishLoading: () => dispatch(finishLoadingUserProjects())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(OpenProjectMenu);