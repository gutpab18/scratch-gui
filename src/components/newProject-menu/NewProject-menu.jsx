import React from "react"
import Modal from "react-modal"
import log from '../../lib/log';
import { connect } from "react-redux"
import { newProjectMenuOpen, closeNewProjectMenu, closeFileMenu } from "../../reducers/menus"
import { requestNewProject, startLoadingUserProjects, finishLoadingUserProjects } from "../../reducers/project-state"
import { setProjectTitle } from "../../reducers/project-title"
import Form from "react-bootstrap/Form"
import { loadTarget } from "../../reducers/targets"
import { closeLoadingProject, openLoadingProject } from '../../reducers/modals';
import createProject from "../../lib/smartcodeStuff/createScratchProject"
import { Formik, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { setSmartcodeProjectId } from "../../reducers/project-state"
import { showAlert } from "../../reducers/alert-ui"
import styles from "./NewProject.css"

const schema = Yup.object().shape({
    projName: Yup.string().min(1, "Too Short!!!").max(20, "Too Long!!!").required('Username is required'),
    projDescription: Yup.string().min(1, "Too Short!!!").max(100, "Too Long!!!").required('Password is required')
});

const initialValues = {
    projName: "",
    projDescription: ""
}

Modal.setAppElement('#main')

const ModalStyle = {
    overlay: {
        position: "absolute",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 1000
    },
    content: {
        position: "absolute",
        top: '50%',
        left: '50%',
        width: '700px',
        transform: 'translate(-50%, -50%)',
        zIndex: 1000
    }
}

const NewProjectMenu = (props) => {

    const {
        alert,
        ProjectMenuOpen,
        closeNewProjectMenu,
        vm,
        onLoadingStarted,
        onLoadingFinished,
        onUpdateProjectTitle,
        closeFileMenu,
        loadingState,
        startLoadingData,
        finishLoadingData,
        loadingProjectData,
        setProjId
    } = props

    const handleClose = () => {
        closeNewProjectMenu()
        closeFileMenu()
    }

    const createBlob = (projCode) => {
        const byteString = atob(projCode)

        // create a blob
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);

        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ab], { type: 'binary/octet-stream' });
    }

    const handleCreateNew = (values, { setStatus }) => {
        
        startLoadingData()

        createProject(values.projName, values.projDescription)
            .then(({ _status, _data }) => {
                finishLoadingData()
                if (_status.code === 701) {
                    alert("Project name already exist!!!!", "Please choose a different name for the project", "failure")
                } else if (_status.code === 401) {
                    alert("Not logged in", "You are not logged in. please login to your account to create a new project", "failure")
                } else if (_status.code !== 200) {
                    alert("Error!!!", " Cannot create new project!!! Please try again later", "failure")
                } else {
                    closeNewProjectMenu()
                    const { projCode, _projID, projName } = _data

                    // create the blob
                    const blob = createBlob(projCode)

                    // load the blob
                    const reader = new FileReader()
                    reader.readAsArrayBuffer(blob);
                    reader.onload = ({ target: { result } }) => {
                        onLoadingStarted()
                        vm.loadProject(result)
                            .then(() => {
                                onLoadingFinished(loadingState, true);
                                closeFileMenu();
                                onUpdateProjectTitle(projName);
                                setProjId(_projID)

                            })
                            .catch(error => {
                                console.log(error)
                                log.warn(error);
                                // Reset the file input after project is loaded
                                // This is necessary in case the user wants to reload a project
                                onLoadingFinished(loadingState, false);
                            });
                    }
                }
            })
    }

    return (
        <div>
            <Modal
                isOpen={ProjectMenuOpen}
                style={ModalStyle}
                onRequestClose={handleClose}
                shouldCloseOnOverlayClick={false}
            >
                <Formik
                    validationSchema={schema}
                    initialValues={initialValues}
                    onSubmit={handleCreateNew}
                >
                    {({ errors, status, isValid, touched,
                        handleSubmit, handleChange,
                        values }) => (
                            <Form onSubmit={handleCreateNew}>
                                <div className={styles.fieldContainer}>
                                    <Form.Row className={styles.textRow}>
                                        <Form.Label> New Project Name </Form.Label>
                                    </Form.Row>
                                    <Form.Row className={styles.row}>
                                        <div className={styles.inputContainer}>
                                            <Form.Control type="plaintext"
                                                placeholder="Enter Project Name"
                                                name="projName"
                                                onChange={handleChange}
                                                isValid={touched.projName && !errors.projName}
                                                values={values.projName}
                                                className={styles.fieldRow}
                                                autoComplete="off"
                                                required
                                            />
                                            <ErrorMessage name="projName" component="div" className={styles.error}/>
                                        </div>
                                    </Form.Row>
                                    <Form.Row className={styles.textRow}>
                                        <Form.Label> Project Description </Form.Label>
                                    </Form.Row>
                                    <Form.Row className={styles.row}>
                                        <div className={styles.inputContainer}>
                                            <Form.Control type="plaintext"
                                                placeholder="Enter Project Description"
                                                name="projDescription"
                                                onChange={handleChange}
                                                isValid={touched.projDescription && !errors.projDescription}
                                                values={values.projDescription}
                                                className={styles.fieldRow}
                                                autoComplete="off"
                                                required
                                            />
                                            <ErrorMessage name="projDescription" component="div" className={styles.error}/>
                                        </div>
                                    </Form.Row>
                                </div>
                                <Form.Row className={styles.buttonContainer}>
                                    <button onClick={handleClose} className={styles.cancelButton}> Cancel </button>
                                    <button 
                                        type="submit" 
                                        className={styles.createNewButton} 
                                        onClick={handleSubmit} 
                                        disabled={loadingProjectData}
                                    > Create New </button>
                                </Form.Row>
                            </Form>

                        )}
                </Formik>

            </Modal>
        </div>
    )
}

const mapStateToProps = state => ({
    ProjectMenuOpen: newProjectMenuOpen(state),
    loadingState: state.scratchGui.projectState.loadingState,
    loadingProjectData: state.scratchGui.projectState.loadingProjectData,
    target: state.scratchGui.targets,
    vm: state.scratchGui.vm
})

const mapDispatchToProps = dispatch => ({
    alert: (header, description, variant) => dispatch(showAlert(header, description, variant)),
    closeNewProjectMenu: () => dispatch(closeNewProjectMenu()),
    closeFileMenu: () => dispatch(closeFileMenu()),
    onClickNew: needSave => dispatch(requestNewProject(needSave)),
    onLoadTarget: targets => dispatch(loadTarget(targets)),
    onLoadingFinished: () => dispatch(closeLoadingProject()),
    onLoadingStarted: () => dispatch(openLoadingProject()),
    onUpdateProjectTitle: title => dispatch(setProjectTitle(title)),
    setProjId: projectId => dispatch(setSmartcodeProjectId(projectId)),
    startLoadingData: () => dispatch(startLoadingUserProjects()),
    finishLoadingData: () => dispatch(finishLoadingUserProjects())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewProjectMenu);