import PropTypes from 'prop-types';
import React from 'react';
import { defineMessages } from 'react-intl';
import { connect } from "react-redux"

import MenuBarMenu from './menu-bar-menu.jsx';
import { MenuItem } from '../menu/menu.jsx';

import styles from './logout-dropdown.css';
import {    startLogout,
            endLogout
        } from '../../reducers/session';
import { closeLogoutMenu } from "../../reducers/menus"
import { showAlert } from "../../reducers/alert-ui"
import Logout from "../../lib/smartcodeStuff/logout"
import { setUsername } from "../../reducers/session"

// these are here as a hack to get them translated, so that equivalent messages will be translated
// when passed in from www via gui's renderLogin() function
const LogoutDropdownMessages = defineMessages({ // eslint-disable-line no-unused-vars
    username: {
        defaultMessage: 'Username',
        description: 'Label for login username input',
        id: 'general.username'
    },
    password: {
        defaultMessage: 'Password',
        description: 'Label for login password input',
        id: 'general.password'
    },
    signin: {
        defaultMessage: 'Sign in',
        description: 'Button text for user to sign in',
        id: 'general.signIn'
    },
    needhelp: {
        defaultMessage: 'Need Help?',
        description: 'Button text for user to indicate that they need help',
        id: 'login.needHelp'
    },
    validationRequired: {
        defaultMessage: 'This field is required',
        description: 'Message to tell user they must enter text in a form field',
        id: 'form.validationRequired'
    }
});

const LogoutDropdown = ({
    className,
    pending,
    isOpen,
    isRtl,
    onStartLogout,
    onEndLogout,
    onRequestCloseLogout,
    setUsername,
    alert
}) => {

        const onLogout = () => {
            Logout().then(({_status, _data}) => {
            
            if (_status.code == 200) {
                alert("Signed Out", "The user has signed out", "success")
                setUsername(null)
            } else {
                alert("Error", "There was an error logging out please try again later", "failure")
            }

            onRequestCloseLogout() 
        })
    }

    return (
        <MenuBarMenu
            className={className}
            open={isOpen}
            // note: the Rtl styles are switched here, because this menu is justified
            // opposite all the others
            place={isRtl ? 'right' : 'left'}
            onRequestClose={onRequestCloseLogout}
        >
            <MenuItem onClick={onLogout}>
                Logout
            </MenuItem>
        </MenuBarMenu >
    )
};

LogoutDropdown.propTypes = {
    className: PropTypes.string,
    isOpen: PropTypes.bool,
    isRtl: PropTypes.bool,
    onClose: PropTypes.func,
    onLogin: PropTypes.func,
    sessionStatus: PropTypes.string
};

const mapStateToProps = state => ({
    pending: state.session.pending
})

const mapDispatchToProps = dispatch => ({   
    onStartLogout: () => dispatch(startLogout()),
    onEndLogout: () => dispatch(endLogout()),
    alert: (header, description, variant) => dispatch(showAlert(header, description, variant)),
    onRequestCloseLogout: () => dispatch(closeLogoutMenu()),
    setUsername: (username) => dispatch(setUsername(username)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LogoutDropdown);
