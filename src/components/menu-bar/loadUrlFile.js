import log from '../../lib/log';
import {defineMessages} from 'react-intl';

const messages = defineMessages({
    loadError: {
        id: 'gui.projectLoader.loadError',
        defaultMessage: 'The project file that was selected failed to load.',
        description: 'An error that displays when a local project file fails to load.'
    }
});

export const loadScratchProjectFromUrl = (
    {vm, onUpdateProjectTitle, intl, onLoadingStarted, onLoadingFinished, loadingState}
) => {
    const reader = new FileReader();
    const request = new XMLHttpRequest();
    request.open('GET', 'https://smartcode-public.s3.us-east-2.amazonaws.com/Star+Wars+Game.sb3', true);
    request.responseType = 'blob';
    request.onload = function () {
        
        reader.readAsArrayBuffer(request.response);
        reader.onload = ({target: {result}}) => {
            onLoadingStarted();
            vm.loadProject(result)
                .then(() => {
                    onLoadingFinished(loadingState, true);
                    // Reset the file input after project is loaded
                    // This is necessary in case the user wants to reload a project
                    const filename = 'Star Wars Game.sb3';
                    if (filename) {
                        onUpdateProjectTitle(filename);
                    }
                })
                .catch(error => {
                    log.warn(error);
                    alert(intl.formatMessage(messages.loadError)); // eslint-disable-line no-alert
                    // Reset the file input after project is loaded
                    // This is necessary in case the user wants to reload a project
                    onLoadingFinished(loadingState, false);
                });
        };
    };
    request.send();
};
