/*
NOTE: this file only temporarily resides in scratch-gui.
Nearly identical code appears in scratch-www, and the two should
eventually be consolidated.
*/
import PropTypes from 'prop-types';
import React from 'react';
import { defineMessages } from 'react-intl';
import { connect } from "react-redux"
import { Formik } from "formik";

import MenuBarMenu from './menu-bar-menu.jsx';
import { MenuItem, MenuItemNoClick, MenuSection } from '../menu/menu.jsx';

import styles from './login-dropdown.css';
import {    startLogin,
            endLogin
        } from '../../reducers/session';
import Spinner from '../spinner/spinner.jsx';
import { FormControl } from "react-bootstrap"
import { openInputLoginMenu, closeInputLoginMenu, inputLoginMenuOpen, closeLoginMenu } from "../../reducers/menus"
import { showAlert } from "../../reducers/alert-ui"
import Login from "../../lib/smartcodeStuff/login"
// these are here as a hack to get them translated, so that equivalent messages will be translated
// when passed in from www via gui's renderLogin() function
const LoginDropdownMessages = defineMessages({ // eslint-disable-line no-unused-vars
    username: {
        defaultMessage: 'Username',
        description: 'Label for login username input',
        id: 'general.username'
    },
    password: {
        defaultMessage: 'Password',
        description: 'Label for login password input',
        id: 'general.password'
    },
    signin: {
        defaultMessage: 'Sign in',
        description: 'Button text for user to sign in',
        id: 'general.signIn'
    },
    needhelp: {
        defaultMessage: 'Need Help?',
        description: 'Button text for user to indicate that they need help',
        id: 'login.needHelp'
    },
    validationRequired: {
        defaultMessage: 'This field is required',
        description: 'Message to tell user they must enter text in a form field',
        id: 'form.validationRequired'
    }
});

const LoginDropdown = ({
    className,
    pending,
    isOpen,
    isRtl,
    openLoginInput,
    inputLoginOpen,
    onRequestCloseLogin,
    onRequestCloseInput,
    onStartLogin,
    onEndLogin,
    alert
}) => {

    const onClose = () => {
        onRequestCloseLogin()
        onRequestCloseInput()
    }

    const onSubmit = (values) => {
        const {username, password} = values
        if (username == "" || password == "") {
            alert("Empty Credentials", "Either your username or password were empty when trying to login", "failure")
            onClose()
            return
        } 

        onStartLogin()
        Login(username, password).then(({_status, _data}) => {
        
            onEndLogin(_data)
            onClose()

            if (_status.code == -3) {
                alert("Invalid Credentials", "There was an invalid username and/or password", "failure")
            } else if(_status.code == 200) {
                alert("Login Successful", "You have logged in successfully", "success")
            } else {
                alert("Error", "There was an error logging in please try again later", "failure")
            }            
        })
    }

    return (
        <MenuBarMenu
            className={className}
            open={isOpen}
            // note: the Rtl styles are switched here, because this menu is justified
            // opposite all the others
            place={isRtl ? 'right' : 'left'}
            onRequestClose={onClose}
        >
            <div>
                {pending ?
                    <Spinner /> :
                    <MenuSection>
                        <Formik 
                            initialValues={{
                                username: "",
                                password: ""
                            }}
                            onSubmit={onSubmit}
                        >
                            {({ values, handleChange, handleSubmit }) => (
                                <div>
                                    {/* eslint-disable-next-line react/jsx-no-bind */}
                                    < MenuItem onClick={openLoginInput}>{`Login`}</MenuItem>
                                    {inputLoginOpen ?
                                        <div>
                                            <MenuItemNoClick> Username </MenuItemNoClick>
                                            <FormControl
                                                type="plaintext"
                                                placeholder="Username"
                                                name="username"
                                                onChange={handleChange}
                                                values={values.username}
                                                autoComplete="off"
                                                required
                                            />
                                            <MenuItemNoClick> Password </MenuItemNoClick>
                                            <FormControl
                                                type="password"
                                                placeholder="Password"
                                                name="password"
                                                onChange={handleChange}
                                                values={values.password}
                                                autoComplete="off"
                                                required
                                            />
                                            <MenuItem className={styles.submit} onClick={handleSubmit}> Submit </MenuItem>
                                        </div>
                                        : null}
                                </div>
                            )}
                        </Formik>
                    </MenuSection>
                }
            </div>
        </MenuBarMenu >
    )
};

LoginDropdown.propTypes = {
    className: PropTypes.string,
    isOpen: PropTypes.bool,
    isRtl: PropTypes.bool,
    onClose: PropTypes.func,
    onLogin: PropTypes.func,
    sessionStatus: PropTypes.string
};

const mapStateToProps = state => ({
    inputLoginOpen: inputLoginMenuOpen(state),
    pending: state.session.pending
})

const mapDispatchToProps = dispatch => ({   
    onStartLogin: () => dispatch(startLogin()),
    onEndLogin: (username) => dispatch(endLogin(username)),
    alert: (header, description, variant) => dispatch(showAlert(header, description, variant)),
    onRequestCloseLogin: () => dispatch(closeLoginMenu()),
    onRequestCloseInput: () => dispatch(closeInputLoginMenu()),
    openLoginInput: () => dispatch(openInputLoginMenu())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginDropdown);
